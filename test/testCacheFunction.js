import cacheFunction from "../cacheFunction.js";

let cache = cacheFunction(function cb(element) {
    return element+10;
});
console.log(cache(2));
console.log(cache(3));
console.log(cache(2));