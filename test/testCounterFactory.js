import counterFactory from "../counterFactory.js";

let result;

try{
    result = counterFactory(2);
} catch(e) {
    console.log(e);
}

console.log(result.increment());