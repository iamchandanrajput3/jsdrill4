function limitFunctionCallCount(cb,n) {

    if(typeof cb !== 'function' || !Number.isInteger(n) || typeof n === 'undefined')
    {
        throw new Error("Invalid Arguments");
    }
    
    function limit(num) {

        for(let i=0;i<n;i++){
            cb(num);
        }
    }
    return limit;
}

export default limitFunctionCallCount;
