function cacheFunction(cb) {

    if(typeof cb !== 'function'){
        throw new Error("Invalid Argument passed");
    }

    let cache = [];

    function onlyOne(arg) {
        for(let index=0;index<cache.length;index++)
        {
            if(cache[index]['argument'] === arg)
            return cache[index]['result'];
        }
        cache.push({'argument': arg, 'result': cb(arg)});
        return cache;
    }
    return onlyOne;
}

export default cacheFunction;
