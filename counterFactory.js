function counterFactory(count) {

    if(!Number.isInteger(count)){
        throw new Error("Invalid Argument");
    }

    let counter = count;

    function increment() {
        return ++counter;
    }
    function decrement() {
        return --counter;
    }

    return {increment,decrement};
}

export default counterFactory;
